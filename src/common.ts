/*
 * Copyright (C) 2019, 2020 Karl-Philipp Wulfert
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {Type} from '@krlwlfrt/tsg';
import {createHash} from 'crypto';
import winston from 'winston';

/**
 * Map of types by name
 */
export type TypeMap = Record<string, Type>;

/**
 * SHA 256 hash
 * @param content Content to hash
 * @returns SHA 256 hash of content
 */
export function sha256(content: string): string {
  const hash = createHash('sha256');
  hash.update(content);

  return hash
    .digest()
    .toString('hex');
}

// initialize logger
export const logger = winston.createLogger({
  level: 'debug',
  transports: [
    new (winston.transports.Console)({
      format: winston.format.combine(
        winston.format.colorize({
          all:true,
        }),
        winston.format.printf(
          info => `[${info.level}] ${info.message}`,
        ),
      ),
    }),
  ],
});
