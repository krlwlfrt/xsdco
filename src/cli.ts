/*
 * Copyright (C) 2019, 2020 Karl-Philipp Wulfert
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {generateTypeScript, isEntity, Type} from '@krlwlfrt/tsg';
import {Command} from 'commander';
import {mkdirSync, readFileSync, writeFileSync} from 'fs';
import {resolve} from 'path';
import {logger, sha256} from './common';
import {generateName, generatePropertyTypeFactory} from './examples';
import {extractFromFile} from './extract';
import {generateObjectValue, generateXml} from './generate';

const commander = new Command('xsdco');
const version = JSON
  .parse(
    readFileSync(resolve(__dirname, '..', 'package.json'))
      .toString(),
  )
  .version;

commander
  .version(version)
  .arguments('<path> <outputPath>')
  .action(async (path, outputPath) => {
    const absolutePath = resolve(path);
    const absoluteOutputPath = resolve(outputPath);

    logger.info(`Extracting types from ${absolutePath}.`);

    const types = await extractFromFile(absolutePath);

    // type map
    const typeMap: Record<string, Type> = {};

    for (const type of types) {
      typeMap[type.name] = type;
    }

    const outputDirectory = resolve(absoluteOutputPath, sha256(path));

    mkdirSync(outputDirectory, {recursive: true});

    writeFileSync(resolve(outputDirectory, 'types.ts'), generateTypeScript(
      types,
      generateName,
      generatePropertyTypeFactory(['xs', 'xsd']),
    ));

    for (const type of types.filter(isEntity)) {
      writeFileSync(
        resolve(outputDirectory, `${type.name}.xml`),
        generateXml(generateObjectValue(type, typeMap)),
      );
    }
  });

commander
  .parse(process.argv);
