/*
 * Copyright (C) 2019, 2020 Karl-Philipp Wulfert
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {convertableToString, OptionsV2, parseString} from 'xml2js';

/**
 * Async version of parseString
 * @param xml XML to parse
 * @param options Options for parsing
 * @returns Object, parsed from XML string
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export async function asyncParseString(xml: convertableToString, options: OptionsV2 = {}): Promise<any> {
  return new Promise((resolve, reject) => {
    parseString(xml, options, (err, result) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve(result);
      }
    });
  });
}
