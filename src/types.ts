/*
 * Copyright (C) 2019, 2020 Karl-Philipp Wulfert
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

export type XSDNamespace = 'xsd:' | '';

export interface XSD {
  'xsd:schema': XSDSchema;
}

export interface XSDSchema {
  $: {
    targetNamespace: string;
    /**
     * XML namespaces
     */
    [nameSpaceIdentifier: string]: string;
  };
  'xsd:complexType': XSDComplexType[];
  'xsd:import': XSDImport[];
  'xsd:include': XSDImport[];
  'xsd:simpleType': XSDSimpleType[];
  'xsd:group': XSDGroup[];
}

export interface XSDImport {
  $: {
    namespace: string;
    schemaLocation: string;
  };
}

export interface XSDSimpleType {
  $: {
    name: string;
  };
  'xsd:restriction'?: XSDRestriction[];
}

export interface XSDRestriction {
  $: {
    base: string;
  };
  'xsd:maxLength': XSDRestrictionValue[];
  'xsd:pattern': XSDRestrictionValue[];
}

export interface XSDRestrictionValue {
  $: {
    value: string;
  };
}

export interface XSDComplexType {
  $: {
    name: string;
  };
  'xsd:attribute': XSDAttribute[];
  'xsd:sequence': XSDSequence[];
  'xsd:simpleContent': XSDSimpleContent[];
  'xsd:group': XSDGroupRef[];
}

export interface XSDSequence {
  'xsd:element': (XSDElement | XSDElementRef)[];
  'xsd:group': XSDGroup[];
}

export interface XSDElement {
  $: {
    maxOccurs?: string;
    minOccurs?: string;
    name: string;
    type?: string;
  };
  'xsd:annotation'?: XSDAnnotation[];
  'xsd:complexType'?: XSDComplexType[];
  'xsd:simpleType'?: XSDSimpleType[];
}

export interface XSDElementRef {
  $: {
    ref: string;
  }
}

export interface XSDAnnotation {
  'xsd:documentation': string[];
}

export interface XSDAttribute {
  $: {
    name: string;
    type: string;
    ref: string;
    use: string;
  };
}

export interface XSDGroup {
  $: {
    name: string;
  };
  'xsd:sequence': XSDSequence[];
}

export interface XSDGroupRef {
  $: {
    ref: string;
  };
}

export interface XSDSimpleContent {
  'xsd:extension': XSDExtension[];
}

export interface XSDExtension {
  $: {
    base: string;
  };
}
