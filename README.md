# @krlwlfrt/xsdco

[![pipeline status](https://img.shields.io/gitlab/pipeline/krlwlfrt/xsdco.svg?style=flat-square)](https://gitlab.com/krlwlfrt/xsdco/commits/master) 
[![npm](https://img.shields.io/npm/v/@krlwlfrt/xsdco.svg?style=flat-square)](https://npmjs.com/package/@krlwlfrt/xsdco)
[![license)](https://img.shields.io/npm/l/@krlwlfrt/xsdco.svg?style=flat-square)](https://opensource.org/licenses/MIT)
[![documentation](https://img.shields.io/badge/documentation-online-blue.svg?style=flat-square)](https://krlwlfrt.gitlab.io/xsdco)


Library to generate TypeScript code from an XSD and parse an XML according to the XSD.

Use `extract` or `extractFromFile` to extract a data description from an XSD and `parseXML` to parse an XML according to the extracted data description.

## Documentation

[See documentation](https://krlwlfrt.gitlab.io/xsdco/) for detailed description of usage. 
