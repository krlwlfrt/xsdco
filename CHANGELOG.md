## [0.3.3](https://gitlab.com/krlwlfrt/xsdco/compare/v0.3.2...v0.3.3) (2024-11-15)



## [0.3.2](https://gitlab.com/krlwlfrt/xsdco/compare/v0.3.1...v0.3.2) (2024-11-15)



## [0.3.1](https://gitlab.com/krlwlfrt/xsdco/compare/v0.3.0...v0.3.1) (2024-11-15)



# [0.3.0](https://gitlab.com/krlwlfrt/xsdco/compare/v0.2.0...v0.3.0) (2024-11-14)


### Features

* add handling for groups and element refs ([fc7149c](https://gitlab.com/krlwlfrt/xsdco/commit/fc7149c94590697577ba5bd54a611ed078a73e84)), closes [#2](https://gitlab.com/krlwlfrt/xsdco/issues/2)



# [0.2.0](https://gitlab.com/krlwlfrt/xsdco/compare/v0.1.2...v0.2.0) (2021-02-22)



## [0.1.2](https://gitlab.com/krlwlfrt/xsdco/compare/v0.1.1...v0.1.2) (2020-02-26)


### Bug Fixes

* make async function sync again ([666363c](https://gitlab.com/krlwlfrt/xsdco/commit/666363cb317ca90e9e307024ee78bf8f060b991e))



## [0.1.1](https://gitlab.com/krlwlfrt/xsdco/compare/v0.1.0...v0.1.1) (2020-02-20)


### Bug Fixes

* check for namespace when generating data ([f0261ae](https://gitlab.com/krlwlfrt/xsdco/commit/f0261aedad085af642b482d97bfaa04742a584c1))



# [0.1.0](https://gitlab.com/krlwlfrt/xsdco/compare/v0.0.1...v0.1.0) (2020-02-19)


### Bug Fixes

* correctly generate decimals without fraction digits ([2f7d207](https://gitlab.com/krlwlfrt/xsdco/commit/2f7d207298b6e19098ade0bb6a04aef6afaeca39))


### Features

* add generation of example xml ([9314c90](https://gitlab.com/krlwlfrt/xsdco/commit/9314c9073e96d4f35c669465d2fa6727d154ae14))
* add output path to cli ([9b37232](https://gitlab.com/krlwlfrt/xsdco/commit/9b372322956d4db8bf2710df061a4a08783f528c))



## [0.0.1](https://gitlab.com/krlwlfrt/xsdco/compare/2168f67bf6675cf050ea8c7b29a9fe77675b3a4b...v0.0.1) (2019-05-09)


### Features

* add implementation ([2168f67](https://gitlab.com/krlwlfrt/xsdco/commit/2168f67bf6675cf050ea8c7b29a9fe77675b3a4b))



