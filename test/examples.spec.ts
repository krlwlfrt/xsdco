import {suite, test} from '@testdeck/mocha';
import {expect} from 'chai';
import {generateName, generatePropertyTypeFactory} from '../src/examples';

@suite()
export class ExamplesSpec {
  @test
  public 'generate a name'() {
    expect(generateName({
      name: 'Foo',
      namespace: 'Bar',
    })).to.be.equal('Bar__Foo');
  }

  @test
  public 'generate a property name'() {
    expect(generatePropertyTypeFactory(['Bar'])({
      name: 'Foo',
      namespace: 'Bar',
      type: {
        name: 'Foobar',
        namespace: 'Bar',
      },
    })).to.be.equal('Foobar');

    expect(generatePropertyTypeFactory(['xsd'])({
      name: 'Foo',
      namespace: 'Bar',
      type: {
        name: 'Foobar',
        namespace: 'Bar',
      },
    })).to.be.equal('Bar__Foobar');
  }
}
