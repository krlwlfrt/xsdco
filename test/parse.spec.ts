import {suite, test} from '@testdeck/mocha';
import {expect} from 'chai';
import {readFile} from 'fs/promises';
import {resolve} from 'path';
import {extractFromFile} from '../src/extract';
import {parseXML} from '../src/parse';

@suite()
export class ParseSpec {
  @test
  async 'parse XML according to XSD'() {
    const types = await extractFromFile(resolve(__dirname, '..', 'resources', 'note.xsd'));

    const testFile = resolve(__dirname, '..', 'resources', 'note.xml');

    const flattenedXML = await parseXML(await readFile(testFile), types, ['xs']);

    expect(flattenedXML).to.be.deep.equal({
      body: 'Don\'t forget me this weekend!',
      from: 'Jani',
      heading: 'Reminder',
      to: 'Tove',
    });
  }
}
