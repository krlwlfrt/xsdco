import {generateTypeScript, Type} from '@krlwlfrt/tsg';
import {suite, test} from '@testdeck/mocha';
import {expect} from 'chai';
import {join} from 'path';
import * as typescript from 'typescript';
import {generateName, generatePropertyTypeFactory} from '../src/examples';
import {extractFromFile} from '../src/extract';

@suite()
export class ExtractSpec {
  @test
  async 'parse note.xsd'() {
    const types = await extractFromFile(join(__dirname, '..', 'resources', 'note.xsd'));

    expect(types).to.have.length(1);

    const expectedTypes: Type[] = [
      {
        name: 'note',
        namespace: '',
        properties: [
          {
            description: undefined,
            multiple: false,
            name: 'to',
            namespace: '',
            required: true,
            type: {
              name: 'string',
              namespace: 'xs',
            },
          },
          {
            description: undefined,
            multiple: false,
            name: 'from',
            namespace: '',
            required: true,
            type: {
              name: 'string',
              namespace: 'xs',
            },
          },
          {
            description: undefined,
            multiple: false,
            name: 'heading',
            namespace: '',
            required: true,
            type: {
              name: 'string',
              namespace: 'xs',
            },
          },
          {
            description: undefined,
            multiple: false,
            name: 'body',
            namespace: '',
            required: true,
            type: {
              name: 'string',
              namespace: 'xs',
            },
          },
        ],
      },
    ];

    expect(types).to.deep.equal(expectedTypes);

    let code = '';

    expect(() => {
      code = typescript.transpile(generateTypeScript(types, generateName, generatePropertyTypeFactory(['xs'])));
    }).not.to.throw();

    expect(code).not.to.be.equal('');
  }

  @test
  async 'parse group.xsd'() {
    const types = await extractFromFile(join(__dirname, '..', 'resources', 'group.xsd'));

    const expectedTypes = [
      {
        'name': 'order',
        'namespace': '',
        'type': {
          'name': 'ordertype',
          'namespace': '',
        },
      },
      {
        'description': undefined,
        'name': 'ordertype',
        'namespace': '',
        'properties': [
          {
            'description': undefined,
            'name': '$status',
            'namespace': '',
            'required': false,
            'type': {
              'name': 'string',
              'namespace': 'xs',
            },
          },
          {
            'description': undefined,
            'multiple': false,
            'name': 'customer',
            'namespace': '',
            'required': true,
            'type': {
              'name': 'string',
              'namespace': 'xs',
            },
          },
          {
            'description': undefined,
            'multiple': false,
            'name': 'orderdetails',
            'namespace': '',
            'required': true,
            'type': {
              'name': 'string',
              'namespace': 'xs',
            },
          },
          {
            'description': undefined,
            'multiple': false,
            'name': 'billto',
            'namespace': '',
            'required': true,
            'type': {
              'name': 'string',
              'namespace': 'xs',
            },
          },
          {
            'description': undefined,
            'multiple': false,
            'name': 'shipto',
            'namespace': '',
            'required': true,
            'type': {
              'name': 'string',
              'namespace': 'xs',
            },
          },
        ],
      },
    ];

    expect(types).to.deep.equal(expectedTypes);
  }

  @test
  async 'parse element.xsd'() {
    const types = await extractFromFile(join(__dirname, '..', 'resources', 'group.xsd'));

    const expectedTypes = [
      {
        'name': 'order',
        'namespace': '',
        'type': {
          'name': 'ordertype',
          'namespace': '',
        },
      },
      {
        'description': undefined,
        'name': 'ordertype',
        'namespace': '',
        'properties': [
          {
            'description': undefined,
            'name': '$status',
            'namespace': '',
            'required': false,
            'type': {
              'name': 'string',
              'namespace': 'xs',
            },
          },
          {
            'description': undefined,
            'multiple': false,
            'name': 'customer',
            'namespace': '',
            'required': true,
            'type': {
              'name': 'string',
              'namespace': 'xs',
            },
          },
          {
            'description': undefined,
            'multiple': false,
            'name': 'orderdetails',
            'namespace': '',
            'required': true,
            'type': {
              'name': 'string',
              'namespace': 'xs',
            },
          },
          {
            'description': undefined,
            'multiple': false,
            'name': 'billto',
            'namespace': '',
            'required': true,
            'type': {
              'name': 'string',
              'namespace': 'xs',
            },
          },
          {
            'description': undefined,
            'multiple': false,
            'name': 'shipto',
            'namespace': '',
            'required': true,
            'type': {
              'name': 'string',
              'namespace': 'xs',
            },
          },
        ],
      },
    ];

    expect(types).to.deep.equal(expectedTypes);
  }
}
